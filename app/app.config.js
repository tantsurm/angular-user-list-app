angular
  .module('someApp')
  .config(['$routeProvider', ($routeProvider) => {
    $routeProvider
      .when('/users', {
        template: '<user-list></user-list>'
      })
      .when('/users/create', {
        template: '<user-creator></user-creator>'
      })
      .when('/users/:userId', {
        template: '<user-detail></user-detail>'
      })
      .otherwise('/users')
  }])